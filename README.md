# Gitlab workshop

This repo contains instructions for our (@davinkevin and @neonox31) Gitlab workshop.
This documentation is generated using awesome [Antora](https://docs.antora.org/antora/2.0/) project.

Latest documentation could be find here : https://neonox31.gitlab.io/gitlab-workshop

## Setup
Use recommended `yarn` or `npm i` command.

## Build documentation
Use recommended `yarn build` or `npm run build` command.

## Serve documentation
Use recommended `yarn serve` or `npm run serve` command.

Please notice that documentation will be automatically generated and refreshed in your browser.

## Release a new version
Use recommended `yarn release` or `npm run release` command

When a new tag is pushed, C.I will automatically publish new documentation on Gitlab pages.
