import java.lang.IllegalStateException
import java.nio.file.Files
import java.nio.file.Paths
import java.nio.file.Path
import java.time.Instant
import java.util.concurrent.TimeUnit
import java.util.stream.Collectors
import java.util.stream.Collectors.*
import kotlin.random.Random

// OTR: docker run -it --rm -v (pwd):/projects --workdir /projects zenika/alpine-kotlin java -jar /usr/lib/kotlinc/lib/kotlin-compiler.jar -script gitlab-workshop/update.kts

// docker run -it --rm -v (pwd):/projects --workdir /projects zenika/alpine-kotlin bash
// apk add git && java -jar /usr/lib/kotlinc/lib/kotlin-compiler.jar -script gitlab-workshop/update.kts

// java -jar /usr/local/Cellar/kotlin/1.3.50/libexec/lib/kotlin-compiler.jar -script gitlab-workshop/update.kts KEY_GITLAB_COM

fun executeCommand(command: String): String {
    val p = Runtime.getRuntime().exec(command);
    val reader = p.inputStream.bufferedReader();
    p.waitFor();
    try {
        return reader.readText()
    } catch (e: Exception) {
        e.printStackTrace();
    } finally {
        reader.close()
    }
    throw IllegalStateException("Command ends up on error")
}

data class SolutionCommit(val id: String, val desc: String, val ref: String, val key: String) {

    fun pipelineId(): String {
        val now = Instant.now().toEpochMilli()

        /*val p = */Runtime.getRuntime().exec("""git checkout -b br-$now $id""", null, Paths.get("/Users/kevin/workspace/gitlab.com/gitlab-workshop/solutions/").toFile()).apply {
            // val input = inputStream.bufferedReader()
            val error = errorStream.bufferedReader()
            waitFor()
            // input.lines().forEach { println(it) }
            error.lines().forEach { println(it) }
        }

        /*val push = */
        Runtime.getRuntime().exec("""git push --set-upstream origin br-$now""", null, Paths.get("/Users/kevin/workspace/gitlab.com/gitlab-workshop/solutions/").toFile()).apply {
//            val input = inputStream.bufferedReader()
            val error = errorStream.bufferedReader()
            waitFor()
            // input.lines().forEach { println(it) }
            error.lines().forEach { println(it) }
        }

        TimeUnit.SECONDS.sleep(10)

        val jqId: Process = Runtime.getRuntime().exec(arrayOf("/bin/sh",
                "-c", """curl -qs --header "PRIVATE-TOKEN: $key" "https://gitlab.com/api/v4/projects/10424369/pipelines?sha=$id" | jq -r .[0].id""")
                , null, Paths.get("/Users/kevin/workspace/gitlab.com/gitlab-workshop/solutions/").toFile()).apply {
            val error = errorStream.bufferedReader()
            waitFor()
            error.lines().forEach { println(it) }
        }
        val input = jqId.inputStream.bufferedReader()

        return input.readLine().apply { println(this) }
//        return "123"
    }

}
fun toSolutionCommit(commitBlock: String, key: String): SolutionCommit {
    val (id, _, _, desc, ref) = commitBlock.lines().filter { it.trim().isNotEmpty() }
    return SolutionCommit(id,desc.trim(),ref.trim(), key)
}

data class Page(val path: Path, val content: List<String>) {

    fun updateLinks(commits: List<SolutionCommit>): Page {
        val newContent = content
                .map { when {
                    it.startsWith("*Project :*") -> updateProjectLinks(it, commits)
                    it.startsWith("*Pipeline :*") -> updatePipelineLink(it, commits)
                    else -> it
                } }

        return copy(content = newContent)
    }

    private fun updateProjectLinks(projectLine: String, commits: List<SolutionCommit>): String {
        val (_, ref) = projectLine.split(" // ")
        println("working on $ref")
        val commit = commits.first { it.ref == ref.trim() }

        return "*Project :* https://gitlab.com/gitlab-workshop/solutions/blob/${commit.id}/[, window=\"_blank\"] // $ref"
    }

    private fun updatePipelineLink(pipeline: String, commits: List<SolutionCommit>): String {
        val (_, ref) = pipeline.split(" // ")
        println("working on $ref")
        val commit = commits.first { it.ref == ref.trim() }
        val pipelineId = commit.pipelineId()
        return "*Pipeline :* https://gitlab.com/gitlab-workshop/solutions/pipelines/$pipelineId[, window=\"_blank\"] // $ref"
    }
}

println("Starting the update")
val apiKey = args[0]
println("key = $apiKey")

val solutions = Paths.get("/Users/kevin/workspace/gitlab.com/gitlab-workshop/solutions/")
val pages = Files.walk(Paths.get("/Users/kevin/workspace/gitlab.com/gitlab-workshop/gitlab-workshop/src/modules/ci-quest/pages/"))
        .filter { it.fileName.toString().endsWith("adoc") }
        .map { Page(it, Files.readAllLines(it)) }
        .collect(toList())

val commits = executeCommand("git --git-dir /Users/kevin/workspace/gitlab.com/gitlab-workshop/solutions/.git log")
        .split("commit ")
        .filter { it.isNotEmpty() }
        .map { toSolutionCommit(it, apiKey)}

pages
        .map { it.updateLinks(commits) }
        .forEach { Files.write(it.path, it.content) }

println("done")
